def is_triangle(a, b, c):
    # Triangle inequality theorem: the sum of any two sides of a triangle must be greater
    # than the length of the remaining side
    return (a + b > c) and (a + c > b) and (b + c > a)

if __name__ == "__main__":
    print(is_triangle(3, 4, 5))
